# import canvasconnector
# import acmtconnector
import smconnector
import json

from datetime import datetime

now = datetime.now()


def call_canvas_connector():
    canvas = canvasconnector.Source()
    result = canvas.extract_data()

    with open('canvasData.json', 'w') as json_file:
        json.dump(result, json_file)
        print("Saved")
        print("Ended at: ", str(datetime.now().time()))


call_canvas_connector()


#
# def call_acmt_connector():
#     acmt = acmtconnector.Source()
#     result = acmt.extract_data()
#
#
# call_acmt_connector()

def call_sm_connector():
    survey = smconnector.Origin()
    results = survey.sources_data()
    # dt_string = now.strftime("%d/%m/%Y %H:%M:%S")
    # print("Completed at = ", dt_string)
    # # print(results)
    # file_name = uuid.uuid4()
    #
    # with open(f"{file_name}.json", 'w') as json_file:
    #     json.dump(results, json_file)
    #     print("Saved")


call_sm_connector()
