import json
import logging
import uuid
from datetime import datetime

import json_log_formatter
import mysql.connector
import requests as requests

from config import tenants

connection = mysql.connector.connect(user='root', password='D$hakd#kfdh',
                                     host='heanalytics-cluster-instance-1.ctdnpqpiuo8u.us-east-2.rds.amazonaws.com',
                                     port=3306)
db_cursor = connection.cursor()

now = datetime.now()
print("now =", now)

formatter = json_log_formatter.JSONFormatter()
json_handler = logging.FileHandler(filename=datetime.now().strftime('%Y-%d-%m-%H_%M_%S.logs'))
json_handler.setFormatter(formatter)
logger = logging.getLogger('my_json')
logger.addHandler(json_handler)

success_message = {
    "courses": 0,
    "modules": 0,
    "assignments": 0,
    "gradebooks": 0,
    "sections": 0,
    "rollEnrollments": 0,
    "terms": 0,
    "date": ''
}


class Origin:
    def __init__(self):
        print("Source has been created")

    def sources_data(self):
        try:
            return get_sm_data()
        except:
            logger.error('Survey Monkey connector error', exe_info=True)


def get_sm_data():
    dt_string = now.strftime("%d/%m/%Y %H:%M:%S")
    print("Started at = ", dt_string)
    survey_property = {
        'surveyList': [],
        'survey': [],
        'singleResponse': []
    }
    contact_property = {'contacts': []}

    root_url = tenants["surveymonkey"]['url']
    headers = {'Content-Type': 'application/json', 'Authorization': tenants["surveymonkey"]["apikey"]}
    # getting survey list
    page_value = 1
    for pages in range(page_value):

        # getting survey
        survey_url = requests.get((root_url + f"surveys?page={str(pages + 1)}&per_page=1"), headers=headers)
        # fetching 1 survey per page basis
        surveys = survey_url.json()
        if 'last' in surveys['links']: page_value = ((surveys['links']['last'].split('=')[1]).split('&')[0])

        for val in surveys["data"]:
            survey_property["surveyList"].append(val)

            db_cursor.execute(
                "select * from python.survey_responses_flag_table where source_survey_id = '%s'" % val['id'])
            db_output = db_cursor.fetchall()

            if db_output:
                get_total_page_from_flag_table = db_output[0][7]  # total pages value
                get_last_page_from_flag_table = db_output[0][6]  # last page number
                get_response_id_from_flag_table = (db_output[0][5])  # last response id

                #   getting details of each surveys
                survey_detail = requests.get((val['href'] + "/details"), headers=headers)
                surveys_details = survey_detail.json()

                #   saving the survey details
                survey_property["survey"].append(surveys_details)
                #   success_message["surveys"] = success_message["surveys"] + len(surveys_details)

                for temp_page in range(get_last_page_from_flag_table, get_total_page_from_flag_table):
                    survey_detail_response_list = requests.get(
                        (val['href'] + f"/responses?page={temp_page}&per_page=1000"), headers=headers)
                    survey_new_response_lists = survey_detail_response_list.json()
                    for new_respone in survey_new_response_lists['data']:
                        if get_response_id_from_flag_table < int(new_respone['id']):
                            single_person_response = requests.get((new_respone["href"] + "/details"), headers=headers)
                            single_person_response_data = single_person_response.json()
                            survey_property["singleResponse"].append(single_person_response_data)
                            update_flag_table(surveys_details['id'], surveys_details['date_modified'],
                                              surveys_details['response_count'],
                                              single_person_response_data['id'], temp_page)
                # generate json file with survey_property
                file_name = str(uuid.uuid4()) + ".json"
                with open(file_name, 'w') as json_file:
                    json.dump(survey_property, json_file)
                    print(f"{file_name}Saved!")

            elif not db_output:
                #   getting details of each surveys
                survey_detail = requests.get((val['href'] + "/details"), headers=headers)
                surveys_details = survey_detail.json()

                #   saving the survey details
                survey_property["survey"].append(surveys_details)
                #   success_message["surveys"] = success_message["surveys"] + len(surveys_details)

                response_page_value = 1
                for responses_page in range(response_page_value):
                    survey_detail_response_list = requests.get(
                        (val['href'] + f"/responses?page={str(responses_page + 1)}&per_page=4"), headers=headers)
                    survey_detail_response_lists = survey_detail_response_list.json()
                    if 'last' in (survey_detail_response_lists['links']):
                        response_page_value = (
                            (survey_detail_response_lists['links']['last'].split('=')[1]).split('&')[
                                0])  # fetching last page value for responses list
                    for response_data in survey_detail_response_lists['data']:
                        single_person_response = requests.get((response_data["href"] + "/details"), headers=headers)
                        single_person_response_data = single_person_response.json()
                        survey_property["singleResponse"].append(single_person_response_data)
                        insert_flag_table(surveys_details['id'], surveys_details['date_created'],
                                          surveys_details['date_modified'],
                                          surveys_details['response_count'],
                                          single_person_response_data['id'],
                                          responses_page + 1, response_page_value)
                # generate json file with survey_property
                file_name = str(uuid.uuid4()) + ".json"
                with open(file_name, 'w') as json_file:
                    json.dump(survey_property, json_file)
                    print("Saved!")
    ########################################################################################################################
    # for contact details
    assumed_total_page = 6
    last_page = select_contacts_flag()
    if last_page:
        last_scanned_page = last_page[0][1]
        for contact in range(last_scanned_page, assumed_total_page):
            contact_url = requests.get((root_url + f"contacts?page={contact}&per_page=4"), headers=headers)
            contacts = contact_url.json()
            if contacts['data']:
                contact_property["contacts"].extend(contacts['data'])
        update_contacts_flag(contact)
        file_name = str(uuid.uuid4()) + ".json"
        with open(file_name, 'w') as json_file:
            json.dump(contact_property, json_file)
            print(f"{file_name}Saved!")

    elif not last_page:
        for contact in range(1, assumed_total_page):
            contact_url = requests.get((root_url + f"contacts?page={contact}&per_page=1000"), headers=headers)
            contacts = contact_url.json()
            if contacts['data']:
                contact_property["contacts"].extend(contacts['data'])
        insert_contacts_flag(contact)
        file_name = str(uuid.uuid4()) + ".json"
        with open(file_name, 'w') as json_file:
            json.dump(contact_property, json_file)
            print(f"{file_name}Saved!")


def insert_flag_table(survey_id, survey_creation, survey_updated, survey_response_count, response_id,
                      response_created, response_modified, last_page, total_pages):
    try:
        connection = mysql.connector.connect(
            host='heanalytics-cluster-instance-1.ctdnpqpiuo8u.us-east-2.rds.amazonaws.com',
            database='python', user='root', password='D$hakd#kfdh')
        cursor = connection.cursor()
        mySql_insert_query = """INSERT INTO python.survey_responses_flag_table (source_survey_id, survey_created, survey_updated, survey_response_count, survey_response_id, survey_response_last_page, survey_responses_total_page) VALUES (%s, %s, %s,%s, %s, %s, %s) """
        record = (
            survey_id, survey_creation, survey_updated, survey_response_count, response_id, last_page, total_pages)
        cursor.execute(mySql_insert_query, record)
        connection.commit()
        print("Record inserted successfully into flag table")

    except mysql.connector.Error as error:
        print("Failed to insert into MySQL table {}".format(error))

    finally:
        if connection.is_connected():
            cursor.close()
            connection.close()
            print("MySQL connection is closed")


def update_flag_table(survey_id, survey_updated, survey_response_count, response_id, last_page):
    try:
        connection = mysql.connector.connect(
            host='heanalytics-cluster-instance-1.ctdnpqpiuo8u.us-east-2.rds.amazonaws.com', database='python',
            user='root', password='D$hakd#kfdh')
        cursor = connection.cursor()
        mySql_update_query = """UPDATE python.survey_responses_flag_table set survey_updated=%s, survey_response_count=%s, survey_response_id=%s, survey_response_last_page=%s where source_survey_id=%s"""
        input = (survey_updated, survey_response_count, response_id, last_page, survey_id)
        cursor.execute(mySql_update_query, input)
        connection.commit()
        print("Multiple columns updated successfully ")

    except mysql.connector.Error as error:
        print("Failed to update columns of table: {}".format(error))

    finally:
        if connection.is_connected():
            connection.close()
            print("MySQL connection is closed")


def insert_contacts_flag(page):
    try:
        connection = mysql.connector.connect(
            host='heanalytics-cluster-instance-1.ctdnpqpiuo8u.us-east-2.rds.amazonaws.com', database='python',
            user='root', password='D$hakd#kfdh')
        cursor = connection.cursor()
        sql_insert = """INSERT INTO python.survey_contacts_flag_table (latest_page) VALUES ({});""".format(page)
        # record = page
        cursor.execute(sql_insert)
        connection.commit()
        print("Record inserted successfully into contact flag table")

    except mysql.connector.Error as error:
        print("Failed to insert into MySQL table {}".format(error))

    finally:
        if connection.is_connected():
            cursor.close()
            connection.close()
            print("MySQL connection is closed")


def update_contacts_flag(page):
    try:
        connection = mysql.connector.connect(
            host='heanalytics-cluster-instance-1.ctdnpqpiuo8u.us-east-2.rds.amazonaws.com', database='python',
            user='root', password='D$hakd#kfdh')
        cursor = connection.cursor()
        mySql_update_query = """UPDATE python.survey_contacts_flag_table set latest_page = {} where idsurvey_contacts_flag_table = 1""".format(
            page)
        # input = page
        cursor.execute(mySql_update_query, page)
        connection.commit()
        print("latest_page column updated successfully ")

    except mysql.connector.Error as error:
        print("Failed to update columns of table: {}".format(error))

    finally:
        if connection.is_connected():
            connection.close()
            print("MySQL connection is closed")


def select_contacts_flag():
    try:
        connection = mysql.connector.connect(
            host='heanalytics-cluster-instance-1.ctdnpqpiuo8u.us-east-2.rds.amazonaws.com', database='python',
            user='root', password='D$hakd#kfdh')
        cursor = connection.cursor()
        cursor.execute("SELECT * FROM python.survey_contacts_flag_table")
        records = cursor.fetchall()
        print("Total number of rows in table: ", cursor.rowcount)
        return records


    except mysql.connector.Error as error:
        print("Error reading data from MySQL table", error)
    finally:
        if connection.is_connected():
            connection.close()
            cursor.close()
            print("MySQL connection is closed")
