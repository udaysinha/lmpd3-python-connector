import json, uuid, logging, boto3
from config import tenants, aws_access
import requests as requests
from datetime import datetime
import json_log_formatter

# s3 = boto3.resource(
#     's3',
#     aws_access_key_id=aws_access["apikeys"]["accessKeyId"],
#     aws_secret_access_key=aws_access["apikeys"]["secretAccessKey"],
#     region_name=aws_access["apikeys"]["region"]
# )
bucket_name = "lmpd3-python-he-acmt"
formatter = json_log_formatter.JSONFormatter()
json_handler = logging.FileHandler(filename=datetime.now().strftime('%Y-%d-%m-%H_%M_%S.logs'))
json_handler.setFormatter(formatter)
logger = logging.getLogger('my_json')
logger.addHandler(json_handler)


class Source:
    def __init__(self):
        print("Source has been created")

    def extract_data(self):
        try:
            return get_acmt_data()
        except:
            logger.error('ACMT connector error', exc_info=True)


def get_acmt_data():
    root_url = tenants["acmt"]["url"]
    headers = {'Content_Type': "application/json"}
    acmt_url_response = requests.get(root_url + "CFDocuments", headers=headers)
    cf_document_data = acmt_url_response.json()

    for cf_doc in cf_document_data["CFDocuments"]:
        # print(cf_doc)
        cf_doc_url = cf_doc["CFPackageURI"]["uri"]  # taking the value of CFPackage URI
        cf_doc_url_response = requests.get(cf_doc_url, headers=headers)
        # getting acmt details
        acmt_details = {'acmt': [],
                        'ACMTcustomfields': []
                        }
        response = cf_doc_url_response.json()
        acmt_details["acmt"].append(response)  # saving the details of published taxonomy
        irm_url = tenants["acmt"]["case_url"] + cf_doc["CFPackageURI"]["identifier"] # case url for IRM Mapping
        cf_metadata = requests.get(irm_url, headers=headers)
        metadata_response = cf_metadata.json()
        acmt_details["ACMTcustomfields"].append(metadata_response["ACMTcustomfields"])  # IRM data
        file_name = str(uuid.uuid4()) + ".json"
        # json_body = bytes(json.dumps(acmt_details).encode('UTF-8'))
        # s3.Bucket(bucket_name).put_object(Key=file_name, Body=json_body)
        # print("saved in s3")

        with open(file_name, 'w') as json_file:
            json.dump(acmt_details, json_file)
            print("Saved!")

    # return acmt_details
