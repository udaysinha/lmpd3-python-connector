import requests as requests
from config import tenants, aws_access
from datetime import datetime
import logging
import re
import json_log_formatter
import mysql.connector
import boto3
import json
import uuid

connection = mysql.connector.connect(user="root", password="1p327E43KY", host="127.0.0.1", port=3306, auth_plugin="mysql_native_password")
db_cursor = connection.cursor()

s3 = boto3.resource(
    's3',
    aws_access_key_id=aws_access["apikeys"]["accessKeyId"],
    aws_secret_access_key=aws_access["apikeys"]["secretAccessKey"],
    region_name=aws_access["apikeys"]["region"]
)
bucket_name = "lmpd3-python-he-canvas"

formatter = json_log_formatter.JSONFormatter()
json_handler = logging.FileHandler(filename=datetime.now().strftime('%Y-%d-%m-%H_%M_%S.logs'))
json_handler.setFormatter(formatter)
logger = logging.getLogger('my_json')
logger.addHandler(json_handler)
success_message = {
    "courses": 0,
    "modules": 0,
    "module_items": 0,
    "assignments": 0,
    "gradebooks": 0,
    "sections": 0,
    "roll_enrollments": 0,
    "terms": 0,
    "date": ''
}

if (tenants["canvas"]['lms']['enable'] == 'true'):
    root_url = tenants["canvas"]['lms']['url']
    headers = {'Content-Type': 'application/json',
               'Authorization': tenants["canvas"]["lms"]["apikey"]}
    course_url_response = requests.get(root_url + "/courses?include=items&per_page=100", headers=headers)
    courses = course_url_response.json()

elif (tenants["canvas"]['altlms']['enable'] == 'true'):
    root_url = tenants["canvas"]['altlms']['url']
    headers = {'Content-Type': 'application/json',
               'Authorization': tenants["canvas"]['altlms']['apikey']}


class Source:
    def __init__(self):
        print("Source has been created")

    def extract_data(self):
        try:
            return get_canvas_data()
        except:
            logger.error('Canvas connector error', exc_info=True)


def get_canvas_data():
    print("Started at: ", str(datetime.now().time()))

    # getting courses
    courses = []
    accounts_url_response = requests.get(root_url + "/accounts", headers=headers)
    accounts = accounts_url_response.json()
    course_url = root_url + "/accounts/" + str(accounts[0]["id"]) + "/courses?include=items&per_page=100"
    course_url_response = requests.get(course_url, headers=headers)

    # Finding last page number
    last_link = course_url_response.links["last"]["url"]
    splitted_link = re.split("&", last_link)
    page_split = re.split("=", splitted_link[1])
    last_page = int(page_split[1])

    # Setting the params to start extraction of courses
    query = "SELECT * from he_python.connector_flag_table where end_point='canvas' and tenant='" + tenants["name"] + "'"
    db_cursor.execute(query)
    result = db_cursor.fetchall()
    start_from = 1
    source_course_id = 0
    if len(result) > 0:
        if result[0][5] != None:
            start_from = int(result[0][5])
        if result[0][3] != None:
            source_course_id = int(result[0][3])

    # Iterating over each page to get associated courses
    for i in range(start_from, last_page + 1):
        course_url = root_url + "/accounts/" + str(accounts[0]["id"]) + "/courses?include=items&page=" + str(
            i) + "&per_page=100"
        course_url_response = requests.get(course_url, headers=headers)
        per_page_courses = course_url_response.json()
        courses = list(filter(lambda x: x["workflow_state"] != "unpublished", per_page_courses))
        process_courses(courses, i, len(result)==0, last_page == i, source_course_id)


def process_courses(courses, page_number, is_insert, is_last_page, source_course_id):
    canvas_data = {
        "courses": [],
        "terms": []
    }
    index = 0
    if source_course_id > 0:
        index = [i for i, val in enumerate(courses) if val["id"] == source_course_id]
        index = index[0] + 1

    for i in range(index, len(courses) + 1):
        course = courses[index]
        query = "SELECT * from he_python.connector_flag_table where end_point='canvas' and tenant='" + tenants["name"] + "'"
        db_cursor.execute(query)
        result = db_cursor.fetchall()
        if len(result) > 0:
            is_insert = False
        if is_last_page:
            last_course_id = courses[len(courses) - 1]["id"]

        course_data = {
            "course_id": course["id"],
            "course_uuid": course["uuid"],
            "course_code": course["course_code"],
            "course_name": course["name"],
            "account_id": course["root_account_id"],
            "enrollment_term_id": course["enrollment_term_id"],
            "workflow_state": course["workflow_state"],
            "start_date": course["start_at"],
            "end_date": course["end_at"],
            "created_at": course["created_at"],
            "modules": [],
            "module_items": [],
            "assignments": [],
            "gradebooks": [],
            "sections": [],
            "roll_enrollments": []
        }
        # getting modules
        course_data["modules"] = get_modules(course_data)

        # getting module items
        course_data["module_items"] = get_module_items(course_data)

        # getting assignments
        course_data["assignments"] = get_assignments(course_data)

        # getting gradebooks
        course_data["gradebooks"] = get_gradebooks(course_data)

        # getting sections
        course_data["sections"] = get_sections(course_data)

        # getting enrollments
        course_data["roll_enrollments"] = get_enrollments(course_data)

        canvas_data["courses"].append(course_data)

        # getting terms
        canvas_data["terms"] = get_terms()

        file_name = str(uuid.uuid4()) + ".json"
        json_body = bytes(json.dumps(canvas_data).encode('UTF-8'))
        s3.Bucket(bucket_name).put_object(Key=file_name, Body=json_body)
        print("A course data pushed into s3 bucket")

        if is_last_page:
            if last_course_id == courses[len(courses) - 1]["id"]:
                date = datetime.now()
                sql = "update he_python.connector_flag_table set page_number = NULL,source_course_id = NULL, last_run ='" + date + "' where tenant = '" + \
                      tenants["name"] + "' and end_point = 'canvas'";
            else:
                date = ""
                sql = "update he_python.connector_flag_table set page_number = '" + page_number + "',source_course_id = '" + str(
                    course_data["course_id"]) + "', last_run ='" + date + "' where tenant = '" + tenants[
                          "name"] + "' and end_point = 'canvas'";


        else:
            if is_insert:
                date = datetime.now()
                sql = "insert into he_python.connector_flag_table (tenant, end_point, source_course_id, last_run, page_number) values (" + \
                      tenants["name"] + ", canvas, " + str(course_data["course_id"]) + "," + str(date)+ str(page_number)+ ")"
            else:
                date = ""
                sql = "update he_python.connector_flag_table set page_number = '" + page_number + "',source_course_id = '" + str(
                    course_data["course_id"]) + "' where tenant= '" + tenants["name"] + "' and end_point='canvas'";

        db_cursor.execute(sql)
        canvas_data["courses"] = []
        canvas_data["terms"] = []

    success_message["date"] = str(datetime.now())
    print(success_message)
    print("Finished at: ", str(datetime.now().time()))
    return canvas_data


# function to get module data
def get_modules(course_data):
    module_url = root_url + "/courses/" + \
                 str(course_data["course_id"]) + "/modules?include=items&per_views=10"
    module_details = []
    moduleurl_response = requests.get(module_url, headers=headers)
    modules = moduleurl_response.json()
    module_details = module_details + modules

    while moduleurl_response.links['current']['url'] != moduleurl_response.links['last']['url']:
        moduleurl_response = requests.get(moduleurl_response.links['next']['url'], headers=headers)
        next_page_modules = moduleurl_response.json()
        module_details = module_details + next_page_modules
    success_message["modules"] = success_message["modules"] + len(module_details)
    return module_details


# function to get module items data
def get_module_items(course_data):
    module_item_details = []
    modules = course_data["modules"]
    for module in modules:
        module_id = module["id"]
        module_items_url = root_url + "/courses/" + \
                           str(course_data["course_id"]) + "/modules/" + str(
            module_id) + "/items?include=items&per_page=10"
        moduleitems_url_respose = requests.get(module_items_url, headers=headers)
        modules_items = moduleitems_url_respose.json()
        module_item_details = module_item_details + modules_items

        while moduleitems_url_respose.links['current']['url'] != moduleitems_url_respose.links['last']['url']:
            moduleitems_url_respose = requests.get(moduleitems_url_respose.links['next']['url'], headers=headers)
            next_page_module_items = moduleitems_url_respose.json()
            module_item_details = module_item_details + next_page_module_items
    success_message["module_items"] = success_message["module_items"] + len(module_item_details)
    return module_item_details


def get_assignments(course_data):
    assignments_data = []
    assignment_url = root_url + "/courses/" + \
                     str(course_data["course_id"]) + "/assignments?include=items&per_page=100"
    assignment_url_response = requests.get(assignment_url, headers=headers)
    assignments = assignment_url_response.json()
    for assignment in assignments:
        single_assignment = {
            "assignment_id": assignment["id"],
            "assignment_name": assignment["name"],
            "created_at": assignment["created_at"],
            "updated_at": assignment["updated_at"],
            "submission_types": assignment["submission_types"][0],
            "rubric": assignment.get("rubric"),
            "rubric_details": assignment.get("rubric_settings"),
            "points_possible": assignment["points_possible"]
        }
        assignments_data.append(single_assignment)
    success_message["assignments"] = success_message["assignments"] + len(assignments_data)
    return assignments_data


def get_gradebooks(course_data):
    gradebooks = []
    # assignment_url = root_url + "/courses/" + \
    #                  str(course_data["course_id"]) + "/assignments?include=items&per_page=100"
    #
    # assignment_url_response = requests.get(assignment_url, headers=headers)
    # assignments = assignment_url_response.json()

    for assignment in course_data["assignments"]:
        gradebooks_url = root_url + "/courses/" + str(course_data["course_id"]) + "/assignments/" \
                         + str(assignment["assignment_id"]) + "/submissions?include[]=rubric_assessment"

        gradebooks_url_response = requests.get(gradebooks_url, headers=headers)
        per_page_gradebooks = gradebooks_url_response.json()
        gradebooks = gradebooks + per_page_gradebooks

        # for each_gradebook in per_page_gradebooks:
        #     gradebooks.append(each_gradebook)

        while gradebooks_url_response.links['current']['url'] != gradebooks_url_response.links['last']['url']:
            gradebooks_url_response = requests.get(gradebooks_url_response.links['next']['url'], headers=headers)
            next_page_gradebooks = gradebooks_url_response.json()
            gradebooks = gradebooks + next_page_gradebooks

            # for each_gradebook in next_page_gradebooks:
            #     gradebooks.append(each_gradebook)
    success_message["gradebooks"] = success_message["gradebooks"] + len(gradebooks)
    return gradebooks


def get_sections(course_data):
    section_data = []
    section_url = root_url + "/courses/" + \
                  str(course_data["course_id"]) + "/sections?include=items&per_page=100"
    sections_url_response = requests.get(section_url, headers=headers)
    sections = sections_url_response.json()
    section_data = section_data + sections
    success_message["sections"] = success_message["sections"] + len(section_data)
    return section_data


def get_enrollments(course_data):
    enrollments_data = []
    enrollment_url = root_url + "/courses/" + \
                     str(course_data["course_id"]) + "/enrollments?include=items&per_page=100"
    enrollments_url_response = requests.get(enrollment_url, headers=headers)
    enrollments = enrollments_url_response.json()
    enrollments_data = enrollments_data + enrollments
    success_message["roll_enrollments"] = success_message["roll_enrollments"] + len(enrollments_data)
    return enrollments_data


def get_terms():
    terms_data = []
    accounts_response = requests.get(root_url + "/accounts", headers=headers)
    accounts = accounts_response.json()
    terms_url = root_url + "/accounts/" + str(accounts[0]["id"]) + "/terms?include=items&per_page=100"
    terms_url_response = requests.get(terms_url, headers=headers)
    first_page_terms = terms_url_response.json()
    terms_data = terms_data + first_page_terms["enrollment_terms"]

    while terms_url_response.links['current']['url'] != terms_url_response.links['last']['url']:
        terms_url_response = requests.get(terms_url_response.links['next']['url'], headers=headers)
        next_page_terms = terms_url_response.json()
        terms_data = terms_data + next_page_terms["enrollment_terms"]

    success_message["terms"] = success_message["terms"] + len(terms_data)
    return terms_data
